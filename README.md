# this is a title

This is a paragraph.

## This is a smaller title.

These are some words

### this is an even smaller title.

underneath here i will have a list;

- bulletpoint 1
- bulletpoint 2
-etc

now i am going to make an ordered list;

1. point one
2. point two

### special sections for code

this is how you show code on a markdown.

first example is going to be a code block;

``` bash
# this is a comment in code
# changing directory in bash

cd <folder>
```

This is how make a code in line `git status`

**bold type**

## bash commands ##

ls - shows a list of files/folders in the directory

touch - is used to create files

mkdir -  is used to create a folder

pwd - print working directory is used to show the current directory you are in

cd - is usually followed by <folder name> to change to a specific directory

rm - is used to remove a file

rm -rf - is used to remove a folder

cat - reads the who the file and displays it onto the terminal

## git commands ##

git init - creates a new local repository within a directory

git add - adds file

git commit -m - "commit message" - saves changes you've made on your computer.

git push - uploading what's in your local repository to online one (bitbucket)